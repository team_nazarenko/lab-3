package AirportExample;
import java.util.concurrent.Semaphore;

public class Airport {
    private Semaphore terminalSemaphore;
    private Semaphore gateSemaphore;

    public Airport() {
        // Ініціалізуємо семафори для терміналів і трапів
        terminalSemaphore = new Semaphore(4); // Чотири термінали
        gateSemaphore = new Semaphore(3); // Три трапи
    }

    public void arrivePassenger(Passenger passenger) throws InterruptedException {
        // Пасажир прибуває до аеропорту та чекає на вільний термінал
        terminalSemaphore.acquire();
        System.out.println(passenger.getName() + " прибув до аеропорту і чекає на термінал.");

        // Пасажир використовує термінал для виходу з аеропорту
        System.out.println(passenger.getName() + " вийшов з аеропорту через термінал.");
        terminalSemaphore.release();
    }

    public void departPassenger(Passenger passenger) throws InterruptedException {
        // Пасажир покидає аеропорт через термінал
        System.out.println(passenger.getName() + " покидає аеропорт.");
    }

    public void arriveAirplane(Airplane airplane) throws InterruptedException {
        // Літак прибуває до аеропорту та чекає на вільний трап
        gateSemaphore.acquire();
        System.out.println(airplane.getName() + " прибув до аеропорту і чекає на трап.");

        // Літак використовує трап для виходу пасажирів
        System.out.println(airplane.getName() + " використовує трап для виходу пасажирів.");
        gateSemaphore.release();
    }

    public void departAirplane(Airplane airplane) throws InterruptedException {
        // Літак покидає аеропорт через трап
        System.out.println(airplane.getName() + " покидає аеропорт.");
    }
}

