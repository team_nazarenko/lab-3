package AirportExample;

import java.util.concurrent.TimeUnit;

public class Passenger implements Runnable {
    private String name;
    private Airport airport;

    public Passenger(String name, Airport airport) {
        this.name = name;
        this.airport = airport;
    }

    @Override
    public void run() {
        try {
            // Пасажир прибуває до аеропорту
            airport.arrivePassenger(this);

            // Пасажир чекає на посадку або висадку
            TimeUnit.SECONDS.sleep(2);

            // Пасажир виходить з аеропорту
            airport.departPassenger(this);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    // Забезпечити термінал безпекою
    public String getName() {
        return name;
    }
}