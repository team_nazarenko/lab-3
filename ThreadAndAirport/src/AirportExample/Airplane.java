package AirportExample;
import java.util.concurrent.TimeUnit;

public class Airplane implements Runnable {
    private String name;
    private Airport airport;

    public Airplane(String name, Airport airport) {
        this.name = name;
        this.airport = airport;
    }

    @Override
    public void run() {
        try {
            // Літак прибуває до аеропорту
            airport.arriveAirplane(this);

            // Літак чекає на посадку або висадку пасажирів
            TimeUnit.SECONDS.sleep(3);

            // Літак вибуває з аеропорту
            airport.departAirplane(this);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String getName() {
        return name;
    }
}

