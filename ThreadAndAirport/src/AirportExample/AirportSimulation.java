package AirportExample;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AirportSimulation {
    public static void main(String[] args) {
        // Створюємо аеропорт
        Airport airport = new Airport();

        // Створюємо пул потоків для пасажирів і літаків
        ExecutorService executor = Executors.newFixedThreadPool(10);

        // Створюємо пасажирів і літаки
        for (int i = 1; i <= 20; i++) {
            Passenger passenger = new Passenger("Passenger-" + i, airport);
            Airplane airplane = new Airplane("Airplane-" + i, airport);
            executor.execute(passenger);
            executor.execute(airplane);
        }

        // Зупиняємо пул потоків після завершення всіх завдань
        executor.shutdown();
    }
}