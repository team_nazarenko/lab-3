package Threads;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MutexExample {
    public static void main(String[] args) {
        int numThreads = 5; // Кількість потоків
        int minSleepTime = 1000; // Мінімальний час очікування в мілісекундах
        int maxSleepTime = 3000; // Максимальний час очікування в мілісекундах

        Random rand = new Random();
        List<Thread> threads = new ArrayList<>();

        for (int i = 0; i < numThreads; i++) {
            String threadName = "Thread-" + i;
            int sleepTime = rand.nextInt(maxSleepTime - minSleepTime) + minSleepTime;
            Thread thread = new MutexThread(threadName, sleepTime);
            thread.start();
            threads.add(thread);
        }

        // Чекаємо, доки всі потоки завершаться
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class MutexThread extends Thread {
    private static final Object mutex = new Object();
    private static int count = 0;
    private String name;
    private int sleepTime;

    public MutexThread(String name, int sleepTime) {
        this.name = name;
        this.sleepTime = sleepTime;
    }

    public void run() {
        synchronized(mutex) {
            count++;
            System.out.println(name + " захоплює м'ютекс. Загальна кількість потоків: " + count);
        }

        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        synchronized(mutex) {
            count--;
            System.out.println(name + " звільняє м'ютекс. Загальна кількість потоків: " + count);
        }
    }
}

