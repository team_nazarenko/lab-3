package Threads;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int choice;

        do {
            System.out.println("Оберіть опцію:");
            System.out.println("1. М'ютекс");
            System.out.println("2. Семафор");
            System.out.println("3. Атомарні змінні");
            System.out.println("4. Пул потоків");
            System.out.println("5. Вихід");

            choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    MutexExample.main(null);
                    break;
                case 2:
                    SemaphoreExample.main(null);
                    break;
                case 3:
                    AtomicExample.main(null);
                    break;
                case 4:
                    ThreadPoolExample.main(null);
                    break;
                case 5:
                    System.out.println("Дякую за використання! До побачення.");
                    break;
                default:
                    System.out.println("Некоректний вибір. Спробуйте ще раз.");
            }
        } while (choice != 5);

        scanner.close();
    }
}

