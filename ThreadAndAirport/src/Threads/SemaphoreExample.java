package Threads;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Semaphore;

public class SemaphoreExample {
    public static void main(String[] args) {
        int numThreads = 8; // Кількість потоків
        int minSleepTime = 1000; // Мінімальний час очікування в мілісекундах
        int maxSleepTime = 3000; // Максимальний час очікування в мілісекундах

        Random rand = new Random();
        List<Thread> threads = new ArrayList<>();

        for (int i = 0; i < numThreads; i++) {
            String threadName = "Thread-" + i;
            int sleepTime = rand.nextInt(maxSleepTime - minSleepTime) + minSleepTime;
            Thread thread = new SemaphoreThread(threadName, sleepTime);
            thread.start();
            threads.add(thread);
        }

        // Чекаємо, доки всі потоки завершаться
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class SemaphoreThread extends Thread {
    private static Semaphore semaphore = new Semaphore(4); // Лічильник семафору
    private String name;
    private int sleepTime;

    public SemaphoreThread(String name, int sleepTime) {
        this.name = name;
        this.sleepTime = sleepTime;
    }

    public void run() {
        try {
            semaphore.acquire(); // Спроба захопити семафор
            System.out.println(name + " захопив семафор.");
            Thread.sleep(sleepTime);
            System.out.println(name + " виконав свою роботу і звільнив семафор.");
            semaphore.release(); // Звільнення семафору
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

