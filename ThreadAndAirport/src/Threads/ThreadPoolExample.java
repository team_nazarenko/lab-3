package Threads;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPoolExample {
    public static void main(String[] args) {
        int numThreads = 5; // Кількість потоків у пулі

        // Створюємо пул потоків з певною кількістю потоків
        ExecutorService executor = Executors.newFixedThreadPool(numThreads);

        // Додаємо роботу для кожного потока у пулі
        for (int i = 0; i < numThreads; i++) {
            Runnable worker = new WorkerThread("Thread-" + i);
            executor.execute(worker);
        }

        // Завершуємо роботу пула потоків після виконання усіх завдань
        executor.shutdown();
    }
}

class WorkerThread implements Runnable {
    private String name;

    public WorkerThread(String name) {
        this.name = name;
    }

    public void run() {
        System.out.println("Потік " + name + " почав виконання роботи.");
        try {
            // Симулюємо роботу потока шляхом затримки на випадковий час
            Thread.sleep((long) (Math.random() * 2000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Потік " + name + " завершив виконання роботи.");
    }
}

