package Threads;
import java.util.concurrent.atomic.AtomicInteger;

public class AtomicExample {
    public static void main(String[] args) {
        int numThreads = 3; // Кількість потоків

        for (int i = 0; i < numThreads; i++) {
            String threadName = "Thread-" + i;
            Thread thread = new AtomicThread(threadName);
            thread.start();
        }
    }
}

class AtomicThread extends Thread {
    private static AtomicInteger sharedVariable = new AtomicInteger(0);
    private String name;

    public AtomicThread(String name) {
        this.name = name;
    }

    public void run() {
        for (int i = 0; i < 5; i++) {
            int currentValue = sharedVariable.getAndIncrement();
            System.out.println(name + " змінює спільну змінну на " + currentValue);
        }
    }
}

